### Pull submodule
`git submodule init`
`git submodule update`

### Usage

`zig build run -- [day] [puzzle]`

For example: `zig build run -- 12 2`

The `[puzzle]` param is optional. When not supplied, both puzzles for that day will be run.

