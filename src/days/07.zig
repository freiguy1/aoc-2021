const std = @import("std");
const util = @import("../util.zig");
const debug = std.debug;
const fs = std.fs;

pub fn puzzle1() !void {
    const line = try util.readSingleLine("data/07.txt");
    var crabs = try util.parseDelimitedUnsigned(u32, line, ",", std.heap.page_allocator);
    defer crabs.deinit();

    var answer: u32 = 0;
    const median: u32 = 358; // used web app to find, hee hee
    for (crabs.items) |crab| {
        const difference = if (crab > median) crab - median else median - crab;
        answer += difference;
    }

    debug.print("{d}\n", .{answer});
}

pub fn puzzle2() !void {
    const line = try util.readSingleLine("data/07.txt");
    var crabs = try util.parseDelimitedUnsigned(u32, line, ",", std.heap.page_allocator);
    defer crabs.deinit();

    var answer: u32 = std.math.maxInt(u32);
    var target: u32 = 0;
    var low: u32 = 450;
    while (low < 520) : (low += 1) {
        const new = findFuel(low, crabs);
        if (new < answer) {
            answer = new;
            target = low;
        }
    }

    debug.print("target: {d}, answer: {d}\n", .{ target, answer });
}

fn findFuel(target: u32, crabs: std.ArrayList(u32)) u32 {
    var answer: u32 = 0;
    for (crabs.items) |crab| {
        const difference = if (crab > target) crab - target else target - crab;
        answer += ((difference * difference + difference) / 2);
    }
    return answer;
}
