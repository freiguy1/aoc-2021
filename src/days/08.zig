const std = @import("std");
const util = @import("../util.zig");
const debug = std.debug;
const fs = std.fs;
const ArrayList = std.ArrayList;
const Allocator = std.mem.Allocator;
const Set = std.bit_set.IntegerBitSet;

const Display = struct {
    left: [10][]const u8 = [_][]const u8{""} ** 10,
    right: [4][]const u8 = [_][]const u8{""} ** 4,

    fn parse(line: []const u8) !@This() {
        var iter_1 = std.mem.split(line, " | ");
        var iter_left = std.mem.split(iter_1.next().?, " ");
        var count: u8 = 0;
        var result = Display{};
        while (iter_left.next()) |left_val| {
            result.left[count] = left_val;
            count += 1;
        }

        count = 0;
        var iter_right = std.mem.split(iter_1.next().?, " ");
        while (iter_right.next()) |right_val| {
            result.right[count] = right_val;
            count += 1;
        }
        return result;
    }

    fn findNumber(self: @This()) u32 {
        var one_set: u7 = undefined;
        var four_set: u7 = undefined;
        var seven_set: u7 = undefined;

        for (self.left) |digit_text| {
            switch (digit_text.len) {
                2 => one_set = toBitSet(digit_text),
                4 => four_set = toBitSet(digit_text),
                3 => seven_set = toBitSet(digit_text),
                else => {},
            }
        }

        var result: u32 = 0;
        for (self.right) |digit_text, index| {
            const digit = switch (digit_text.len) {
                2 => @as(u32, 1),
                3 => @as(u32, 7),
                4 => @as(u32, 4),
                7 => @as(u32, 8),
                5 => blk: {
                    const mask = toBitSet(digit_text);
                    break :blk if (one_set & mask == one_set) @as(u32, 3) else if (@popCount(u7, four_set & mask) == 3) @as(u32, 5) else @as(u32, 2);
                },
                6 => blk: {
                    const mask = toBitSet(digit_text);
                    break :blk if (four_set & mask == four_set) @as(u32, 9) else if (one_set & mask == one_set) @as(u32, 0) else @as(u32, 6);
                },
                else => unreachable,
            };
            result = result * 10 + digit;
        }

        return result;
    }
};

fn toBitSet(letters: []const u8) u7 {
    var result = Set(7).initEmpty();
    for (letters) |letter| {
        result.set(letter - 'a');
    }
    return result.mask;
}

pub fn puzzle1() !void {
    var data_file = try fs.cwd().openFile("data/08.txt", .{});
    defer data_file.close();
    var iter = util.FileLineIterator{ .file = data_file };
    var displays = ArrayList(Display).init(std.heap.page_allocator);
    while (iter.next()) |line| {
        try displays.append(try Display.parse(line));
    }

    var answer: u32 = 0;
    for (displays.items) |display| {
        for (display.right) |value| {
            if (value.len == 2 or value.len == 3 or value.len == 4 or value.len == 7) {
                answer += 1;
            }
        }
    }
    debug.print("{d}\n", .{answer});
}

pub fn puzzle2() !void {
    // var data_file = try fs.cwd().openFile("data/08.example.txt", .{});
    var data_file = try fs.cwd().openFile("data/08.txt", .{});
    defer data_file.close();
    var iter = util.FileLineIterator{ .file = data_file };
    var answer: u32 = 0;
    while (iter.next()) |line| {
        const display = try Display.parse(line);
        answer += display.findNumber();
    }

    debug.print("{d}\n", .{answer});
}
