const std = @import("std");
const util = @import("../util.zig");
const debug = std.debug;
const fs = std.fs;
const parseUnsigned = std.fmt.parseUnsigned;

const board_size: u8 = 5;

pub fn puzzle1() !void {
    var data_file = try fs.cwd().openFile("data/04.txt", .{});
    defer data_file.close();

    var iter = util.FileLineIterator{ .file = data_file };
    var buffer: [10_000_000]u8 = undefined;
    var fba = std.heap.FixedBufferAllocator.init(&buffer);
    var allocator = &fba.allocator;
    var boards = std.ArrayList(Board).init(allocator);
    defer boards.deinit();
    var numbers = std.ArrayList(u8).init(allocator);
    defer numbers.deinit();
    var past_numbers = false;
    var next_board = Board{};

    while (iter.next()) |line| {
        if (!past_numbers) {
            var numbers_iter = std.mem.split(line, ",");
            while (numbers_iter.next()) |val| {
                try numbers.append(try parseUnsigned(u8, val, 10));
            }
            past_numbers = true;
            continue;
        }

        if (line.len == 0) {
            continue;
        }

        next_board.addLine(line);
        if (next_board.line_count == 5) {
            try boards.append(next_board);
            next_board = Board{};
        }
    }

    for (numbers.items) |n| {
        for (boards.items) |*b| {
            b.callNumber(n);
            const maybe_win = b.calcWin();
            if (maybe_win != null and !b.has_won) {
                b.has_won = true;
                debug.print("answer: {d}\n", .{maybe_win.? * n});
            }
        }
    }
}

pub fn puzzle2() !void {
    debug.print("Solved in puzzle1() along with puzzle 1\n", .{});
}

const Cell = struct {
    value: u8,
    selected: bool = false,

    fn parse(text: []const u8) @This() {
        return if (text[0] == ' ')
            Cell{ .value = parseUnsigned(u8, text[1..2], 10) catch 0 }
        else
            Cell{ .value = parseUnsigned(u8, text[0..2], 10) catch 0 };
    }
};

const Board = struct {
    has_won: bool = false,
    line_count: u8 = 0,
    values: [board_size][board_size]Cell =
        // [_][board_size]u8{[_]u8{0} ** board_size} ** board_size,
        [_][board_size]Cell{undefined} ** board_size,

    fn addLine(this: *Board, line: []const u8) void {
        var row = this.values[this.line_count];
        row[0] = Cell.parse(line[0..2]);
        row[1] = Cell.parse(line[3..5]);
        row[2] = Cell.parse(line[6..8]);
        row[3] = Cell.parse(line[9..11]);
        row[4] = Cell.parse(line[12..14]);
        this.values[this.line_count] = row;
        this.line_count += 1;
    }

    fn callNumber(this: *Board, n: u8) void {
        for (this.values) |*row| {
            for (row) |*cell| {
                if (cell.value == n) cell.selected = true;
            }
        }
    }

    fn calcWin(this: *const Board) ?u32 {
        for (this.values) |_, x| {
            var x_selected: u8 = 0;
            var y_selected: u8 = 0;
            for (this.values) |_, y| {
                if (this.values[x][y].selected) x_selected += 1;
                if (this.values[y][x].selected) y_selected += 1;
            }

            if (x_selected == board_size) {
                return this.sumOfUnselected();
            }

            if (y_selected == board_size) {
                return this.sumOfUnselected();
            }
        }
        return null;
    }

    fn sumOfUnselected(this: *const Board) u32 {
        var sum: u32 = 0;
        for (this.values) |row| {
            for (row) |cell| {
                if (!cell.selected) sum += cell.value;
            }
        }
        return sum;
    }
};
