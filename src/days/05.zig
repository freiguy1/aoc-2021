const std = @import("std");
const util = @import("../util.zig");
const debug = std.debug;
const fs = std.fs;
const parseUnsigned = std.fmt.parseUnsigned;

const Point = struct {
    x: u32,
    y: u32,
    fn parse(text: []const u8) @This() {
        var iter = std.mem.split(text, ",");
        return Point{
            .x = parseUnsigned(u32, iter.next().?, 10) catch 0,
            .y = parseUnsigned(u32, iter.next().?, 10) catch 0,
        };
    }
};

const Line = struct {
    start: Point,
    end: Point,
    fn parse(text: []const u8) @This() {
        var iter = std.mem.split(text, " ");
        const start = Point.parse(iter.next().?);
        const arrow = iter.next(); // skip arrow
        return Line{
            .start = start,
            .end = Point.parse(iter.next().?),
        };
    }
};

pub fn puzzle1() !void {
    var data_file = try fs.cwd().openFile("data/05.txt", .{});
    defer data_file.close();

    var iter = util.FileLineIterator{ .file = data_file };
    var grid = [_][1000]u8{[_]u8{0} ** 1000} ** 1000;

    while (iter.next()) |text_line| {
        const line = Line.parse(text_line);
        const is_straight = line.start.x == line.end.x or line.start.y == line.end.y;
        if (line.start.x == line.end.x) {
            // vertical
            const lesser_point = if (line.start.y < line.end.y) line.start else line.end;
            const greater_point = if (line.start.y > line.end.y) line.start else line.end;
            var current = lesser_point.y;
            while (current <= greater_point.y) : (current += 1) {
                grid[lesser_point.x][current] += 1;
            }
        } else if (line.start.y == line.end.y) {
            // horizontal
            const lesser_point = if (line.start.x < line.end.x) line.start else line.end;
            const greater_point = if (line.start.x > line.end.x) line.start else line.end;
            var current = lesser_point.x;
            while (current <= greater_point.x) : (current += 1) {
                grid[current][lesser_point.y] += 1;
            }
        }
    }

    var answer: usize = 0;
    for (grid) |row| {
        for (row) |cell| {
            if (cell > 1) answer += 1;
        }
    }

    debug.print("answer: {d}\n", .{answer});
}

pub fn puzzle2() !void {
    var data_file = try fs.cwd().openFile("data/05.txt", .{});
    defer data_file.close();

    var iter = util.FileLineIterator{ .file = data_file };
    var grid = [_][1000]u8{[_]u8{0} ** 1000} ** 1000;

    while (iter.next()) |text_line| {
        const line = Line.parse(text_line);
        const is_straight = line.start.x == line.end.x or line.start.y == line.end.y;
        if (line.start.x == line.end.x) {
            // vertical
            const lesser_point = if (line.start.y < line.end.y) line.start else line.end;
            const greater_point = if (line.start.y > line.end.y) line.start else line.end;
            var current = lesser_point.y;
            while (current <= greater_point.y) : (current += 1) {
                grid[lesser_point.x][current] += 1;
            }
        } else if (line.start.y == line.end.y) {
            // horizontal
            const lesser_point = if (line.start.x < line.end.x) line.start else line.end;
            const greater_point = if (line.start.x > line.end.x) line.start else line.end;
            var current = lesser_point.x;
            while (current <= greater_point.x) : (current += 1) {
                grid[current][lesser_point.y] += 1;
            }
        } else {
            // diagonal
            const lesser_point = if (line.start.x < line.end.x) line.start else line.end;
            const greater_point = if (line.start.x > line.end.x) line.start else line.end;
            const is_y_increasing = lesser_point.y < greater_point.y;
            var to_add: usize = 0;
            while (to_add <= greater_point.x - lesser_point.x) : (to_add += 1) {
                const x = lesser_point.x + to_add;
                const y = if (is_y_increasing) lesser_point.y + to_add else lesser_point.y - to_add;
                grid[x][y] += 1;
            }
        }
    }

    var answer: usize = 0;
    for (grid) |row| {
        for (row) |cell| {
            if (cell > 1) answer += 1;
        }
    }

    debug.print("answer: {d}\n", .{answer});
}
