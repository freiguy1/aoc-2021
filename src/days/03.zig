const std = @import("std");
const util = @import("../util.zig");
const debug = std.debug;
const fs = std.fs;
const parseUnsigned = std.fmt.parseUnsigned;

pub fn puzzle1() !void {
    var data_file = try fs.cwd().openFile("data/03_1.txt", .{});
    defer data_file.close();

    var iter = util.FileLineIterator{ .file = data_file };
    var ones = [_]u32{0} ** 12;
    var lineCount: u32 = 0;

    while (iter.next()) |line| {
        for (line) |char, index| {
            if (char == '1') {
                ones[index] += 1;
            }
        }

        lineCount += 1;
    }

const halfLineCount = lineCount / 2;
    var val1 = [_]u8{0} ** 12;
    var val2 = [_]u8{0} ** 12;
    for (ones) |ones_count, index| {
        if (ones_count > halfLineCount) {
            val1[index] = '1';
            val2[index] = '0';
        } else {
            val1[index] = '0';
            val2[index] = '1';
        }
    }

    debug.print("answer: {d}\n", .{(try parseUnsigned(u32, &val1, 2)) * (try parseUnsigned(u32, &val2, 2))});
}

// const width = 5;
// const length = 12;
const width = 12;
const length = 1000;

pub fn puzzle2() !void {
    var data_file = try fs.cwd().openFile("data/03_1.txt", .{});
    defer data_file.close();

    var iter = util.FileLineIterator{ .file = data_file };

    var full_list: [length][width]u8 = undefined;
    var index: u32 = 0;
    while (iter.next()) |line| {
        std.mem.copy(u8, &full_list[index], line);
        index += 1;
    }

    var buffer: [length * width * 3]u8 = undefined;
    var fba = std.heap.FixedBufferAllocator.init(&buffer);
    var allocator = &fba.allocator;

    const oxygen = try findO2Co2(true, allocator, full_list);
    fba.reset();
    const co2 = try findO2Co2(false, allocator, full_list);

    debug.print("{d} * {d} = {d}\n", .{ oxygen, co2, oxygen * co2 });
}

fn findO2Co2(keep_most_common: bool, allocator: *std.mem.Allocator, full_list: [length][width]u8) !u32 {
    var current_list = try std.ArrayList([width]u8).initCapacity(allocator, length);
    defer current_list.deinit();
    try current_list.appendSlice(&full_list);

    var iteration: u8 = 0;

    while (current_list.items.len != 1) {
        const most_common = mostCommon(current_list.items, iteration);
        var new_list = std.ArrayList([width]u8).init(allocator);
        for (current_list.items) |i| {
            if ((keep_most_common and i[iteration] == most_common) or (!keep_most_common and i[iteration] != most_common)) {
                try new_list.append(i);
            }
        }
        current_list.deinit();
        current_list = new_list;
        iteration += 1;
    }

    return try parseUnsigned(u32, &current_list.items[0], 2);
}

fn mostCommon(values: [][width]u8, column: u8) u8 {
    var ones_count: u32 = 0;
    for (values) |value| {
        if (value[column] == '1') {
            ones_count += 1;
        }
    }
    const zeros_count = values.len - ones_count;

    if (zeros_count > ones_count) {
        return '0';
    } else {
        return '1';
    }
}
