const std = @import("std");
const util = @import("../util.zig");
const debug = std.debug;
const fs = std.fs;

pub fn puzzle1() !void {
    var data_file = try fs.cwd().openFile("data/01_1.txt", .{});
    defer data_file.close();

    var iter = util.FileLineUintIterator.new(data_file);
    var previous: u32 = 0xFFFF_FFFF;
    var answer: u32 = 0;
    while (iter.next()) |num| {
        if (previous < num)
            answer += 1;
        previous = num;
    }
    debug.print("answer: {d}\n", .{answer});
}

pub fn puzzle2() !void {
    var data_file = try fs.cwd().openFile("data/01_1.txt", .{});
    defer data_file.close();

    var iter = util.FileLineUintIterator.new(data_file);
    var previous = [_]?u32{null} ** 4;
    var answer: u32 = 0;
    while (iter.next()) |num| {
        previous[0] = previous[1];
        previous[1] = previous[2];
        previous[2] = previous[3];
        previous[3] = num;

        if (previous[0] == null) {
            continue;
        }

        const middle_two = previous[1].? + previous[2].?;
        if (previous[0].? + middle_two < middle_two + previous[3].?)
            answer += 1;
    }
    debug.print("answer: {d}\n", .{answer});
}
