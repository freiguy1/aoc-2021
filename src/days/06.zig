const std = @import("std");
const util = @import("../util.zig");
const debug = std.debug;
const fs = std.fs;
const parseUnsigned = std.fmt.parseUnsigned;

pub fn parse(allocator: *std.mem.Allocator, capacity: u32) !std.ArrayList(u8) {
    var data_file = try fs.cwd().openFile("data/06.txt", .{});
    defer data_file.close();
    var iter = util.FileLineIterator{ .file = data_file };
    var numbers = try std.ArrayList(u8).initCapacity(allocator, capacity);
    const line = iter.next().?;
    var number_iter = std.mem.split(line, ",");
    while (number_iter.next()) |val| {
        try numbers.append(try parseUnsigned(u8, val, 10));
    }
    return numbers;
}

pub fn puzzle1() !void {
    const capacity: u32 = 1_000;
    var buffer: [capacity]u8 = undefined;
    var fba = std.heap.FixedBufferAllocator.init(&buffer);
    var allocator = &fba.allocator;
    var fish = try parse(allocator, capacity);
    defer fish.deinit();
    var fish_counts = [_]u64{0} ** 9;

    for (fish.items) |age| {
        fish_counts[age] += 1;
    }

    var day: usize = 0;
    while (day < 80) : (day += 1) {
        const zero_count = fish_counts[0];
        for (fish_counts) |_, index| {
            if (index == 8) {
                fish_counts[8] = zero_count;
            } else if (index == 6) {
                fish_counts[6] = zero_count + fish_counts[7];
            } else {
                fish_counts[index] = fish_counts[index + 1];
            }
        }
    }

    var answer: u64 = 0;
    for (fish_counts) |count| {
        answer += count;
    }
    debug.print("{d}\n", .{answer});
}

pub fn puzzle2() !void {
    const capacity: u32 = 1_000;
    var buffer: [capacity]u8 = undefined;
    var fba = std.heap.FixedBufferAllocator.init(&buffer);
    var allocator = &fba.allocator;
    var fish = try parse(std.heap.page_allocator, capacity);
    defer fish.deinit();
    var fish_counts = [_]u64{0} ** 9;

    for (fish.items) |age| {
        fish_counts[age] += 1;
    }

    var day: usize = 0;
    while (day < 256) : (day += 1) {
        const zero_count = fish_counts[0];
        for (fish_counts) |_, index| {
            if (index == 8) {
                fish_counts[8] = zero_count;
            } else if (index == 6) {
                fish_counts[6] = zero_count + fish_counts[7];
            } else {
                fish_counts[index] = fish_counts[index + 1];
            }
        }
    }

    var answer: u64 = 0;
    for (fish_counts) |count| {
        answer += count;
    }
    debug.print("{d}\n", .{answer});
}
