const std = @import("std");
const util = @import("../util.zig");
const debug = std.debug;
const fs = std.fs;

pub fn puzzle1() !void {
    var data_file = try fs.cwd().openFile("data/02_1.txt", .{});
    defer data_file.close();

    var iter = util.FileLineIterator{ .file = data_file };
    var horizontal: u32 = 0;
    var depth: u32 = 0;

    while (iter.next()) |line| {
        var line_iter = std.mem.split(line, " ");
        var direction = line_iter.next().?;
        var distance = std.fmt.parseUnsigned(u32, line_iter.next().?, 10) catch continue;

        if (std.mem.eql(u8, direction, "down")) {
            depth += distance;
        } else if (std.mem.eql(u8, direction, "up")) {
            depth -= distance;
        } else if (std.mem.eql(u8, direction, "forward")) {
            horizontal += distance;
        }
    }

    debug.print("h: {d}, d: {d}, hxd: {d}\n", .{ horizontal, depth, horizontal * depth });
}

pub fn puzzle2() !void {
    var data_file = try fs.cwd().openFile("data/02_1.txt", .{});
    defer data_file.close();

    var iter = util.FileLineIterator{ .file = data_file };
    var horizontal: u32 = 0;
    var depth: u32 = 0;
    var aim: u32 = 0;

    while (iter.next()) |line| {
        var line_iter = std.mem.split(line, " ");
        var direction = line_iter.next().?;
        var distance = std.fmt.parseUnsigned(u32, line_iter.next().?, 10) catch continue;

        if (std.mem.eql(u8, direction, "down")) {
            aim += distance;
        } else if (std.mem.eql(u8, direction, "up")) {
            aim -= distance;
        } else if (std.mem.eql(u8, direction, "forward")) {
            horizontal += distance;
            depth += aim * distance;
        }
    }

    debug.print("h: {d}, d: {d}, hxd: {d}\n", .{ horizontal, depth, horizontal * depth });
}
