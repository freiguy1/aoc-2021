const std = @import("std");
const util = @import("../util.zig");
const debug = std.debug;
const fs = std.fs;
const ArrayList = std.ArrayList;
const Allocator = std.mem.Allocator;
const Set = std.bit_set.IntegerBitSet;
const BoundedArray = std.BoundedArray;

const opens = [_]u8{ '(', '[', '{', '<' };
const closes = [_]u8{ ')', ']', '}', '>' };

fn isOpenBracket(bracket: u8) bool {
    for (opens) |open| {
        if (open == bracket) return true;
    }
    return false;
}

fn findPair(bracket: u8) u8 {
    for (opens) |open, index| {
        if (open == bracket) return closes[index];
    }

    for (closes) |close, index| {
        if (close == bracket) return opens[index];
    }

    unreachable;
}

fn illegalScore(bracket: u8) u32 {
    return switch (bracket) {
        ')' => 3,
        ']' => 57,
        '}' => 1197,
        '>' => 25137,
        else => unreachable,
    };
}

fn closingScore(bracket: u8) u32 {
    return switch (bracket) {
        ')' => 1,
        ']' => 2,
        '}' => 3,
        '>' => 4,
        else => unreachable,
    };
}

pub fn puzzle1() !void {
    var data_file = try fs.cwd().openFile("data/10.txt", .{});
    defer data_file.close();
    var iter = util.FileLineIterator{ .file = data_file };
    var stack = ArrayList(u8).init(std.heap.page_allocator);
    defer stack.deinit();
    var answer: u32 = 0;
    while (iter.next()) |line| {
        try stack.resize(0);
        for (line) |char| {
            const isOpen = isOpenBracket(char);
            if (isOpen) try stack.append(char) else {
                if (stack.items.len == 0) {
                    // debug.print("Too many closing brackets: {c}\n", .{char});
                    break;
                }
                const pair = findPair(char);
                if (stack.pop() != pair) {
                    answer += illegalScore(char);
                    // debug.print("incorrect closing bracket: {c}\n", .{char});
                    break;
                }
            }
        }
    }
    debug.print("answer: {d}\n", .{answer});
}

const u64asc = std.sort.asc(u64);

pub fn puzzle2() !void {
    var data_file = try fs.cwd().openFile("data/10.txt", .{});
    defer data_file.close();
    var iter = util.FileLineIterator{ .file = data_file };
    var stack = ArrayList(u8).init(std.heap.page_allocator);
    defer stack.deinit();

    var scores = ArrayList(u64).init(std.heap.page_allocator);
    defer scores.deinit();

    outer: while (iter.next()) |line| {
        try stack.resize(0);
        for (line) |char| {
            const isOpen = isOpenBracket(char);
            if (isOpen) try stack.append(char) else {
                if (stack.items.len == 0) {
                    // debug.print("Too many closing brackets: {c}\n", .{char});
                    continue :outer;
                }
                const pair = findPair(char);
                if (stack.pop() != pair) {
                    // debug.print("incorrect closing bracket: {c}\n", .{char});
                    continue :outer;
                }
            }
        }

        if (stack.items.len != 0) {
            var score: u64 = 0;
            while (stack.items.len > 0) {
                const open = stack.pop();
                score = score * 5 + closingScore(findPair(open));
            }
            try scores.append(score);
        }
    }

    std.sort.sort(u64, scores.items, {}, u64asc);
    debug.print("answer: {d}\n", .{scores.items[scores.items.len / 2]});
}
