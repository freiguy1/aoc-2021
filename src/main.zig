const clap = @import("clap");
const std = @import("std");
const day_empty = @import("days/empty.zig");
const day_01 = @import("days/01.zig");
const day_02 = @import("days/02.zig");
const day_03 = @import("days/03.zig");
const day_04 = @import("days/04.zig");
const day_05 = @import("days/05.zig");
const day_06 = @import("days/06.zig");
const day_07 = @import("days/07.zig");
const day_08 = @import("days/08.zig");

const day_10 = @import("days/10.zig");

const debug = std.debug;
const io = std.io;

const puzzles = [_][2]fn () anyerror!void{
    [2]fn () anyerror!void{ day_01.puzzle1, day_01.puzzle2 },
    [2]fn () anyerror!void{ day_02.puzzle1, day_02.puzzle2 },
    [2]fn () anyerror!void{ day_03.puzzle1, day_03.puzzle2 },
    [2]fn () anyerror!void{ day_04.puzzle1, day_04.puzzle2 },
    [2]fn () anyerror!void{ day_05.puzzle1, day_05.puzzle2 },
    [2]fn () anyerror!void{ day_06.puzzle1, day_06.puzzle2 },
    [2]fn () anyerror!void{ day_07.puzzle1, day_07.puzzle2 },
    [2]fn () anyerror!void{ day_08.puzzle1, day_08.puzzle2 },
    [2]fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]fn () anyerror!void{ day_10.puzzle1, day_10.puzzle2 },
    [2]fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
};

pub fn main() anyerror!void {
    const params = comptime [_]clap.Param(clap.Help){
        clap.parseParam("-h, --help  Display this help and exit.") catch unreachable,
        clap.parseParam("<POS>...") catch unreachable,
    };

    var args = try clap.parse(clap.Help, &params, .{});
    defer args.deinit();

    if (args.flag("--help")) {
        debug.print("Usage: aoc-2021 <options> [day] [puzzle-number]\n\tday: required (1-25)\n\tpuzzle-number: optional [1|2]\n\nGeneral help:\n", .{});
        return clap.help(io.getStdErr().writer(), &params);
    }

    const dayNum = try parseDayNum(args.positionals());
    const puzzleNum = try parsePuzzleNum(args.positionals());
    if (puzzleNum != null and puzzleNum.? > 2) {
        debug.print("Puzzle number too high\n", .{});
        return error.InvalidPuzzleNum;
    }

    if (dayNum != null and dayNum.? > puzzles.len) {
        debug.print("Day number too high\n", .{});
        return error.InvalidDayNum;
    }

    // Run one or more puzzles using args
    if (dayNum) |dn| {
        if (puzzleNum) |pn| {
            try puzzles[dn - 1][pn - 1]();
        } else {
            try puzzles[dn - 1][0]();
            try puzzles[dn - 1][1]();
        }
    } else {
        for (puzzles) |puzzle| {
            try puzzle[0]();
            try puzzle[1]();
        }
    }
}

fn parseDayNum(positionals: []const []const u8) !?u8 {
    if (positionals.len == 0)
        return null;

    return std.fmt.parseUnsigned(u8, positionals[0], 10) catch return error.InvalidDayNum;
}

fn parsePuzzleNum(positionals: []const []const u8) !?u8 {
    if (positionals.len < 2)
        return null;

    return std.fmt.parseUnsigned(u8, positionals[1], 10) catch return error.InvalidPuzzleNum;
}
